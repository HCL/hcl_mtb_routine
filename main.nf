#!/usr/bin/env nextflow
/*
========================================================================================
    HCL_MTB_routine
========================================================================================
    Gitlab : https://gitlab.inria.fr/NGS/hcl_mtb_routine
----------------------------------------------------------------------------------------
*/

nextflow.enable.dsl = 2

/*
 * Default pipeline parameters. They can be overriden on the command line eg.
 * given `params.foo` specify on the run command line `--foo some_value`.
 */

params.reads = "input/*_R{1,2}.fastq"
params.outdir = "results"


log.info """\
 \033[0;34m-----------------------------------------------------------------\033[0;36m
  _  _  ___ _      __  __ _____ ___                _   _          
 | || |/ __| |    |  \\/  |_   _| _ )  _ _ ___ _  _| |_(_)_ _  ___ 
 | __ | (__| |__  | |\\/| | | | | _ \\ | '_/ _ \\ || |  _| | ' \\/ -_)
 |_||_|\\___|____| |_|  |_| |_| |___/ |_| \\___/\\_,_|\\__|_|_||_\\___|
                                                                  
 \033[0;34m-----------------------------------------------------------------\033[0m
 reads        : ${params.reads}
 outdir       : ${params.outdir}
 """

// Create Channel From Input
Channel
    .fromFilePairs( params.reads, checkExists:true )
    .set{ read_pairs_ch }

// Modules inclusion
include { FASTQC } from './modules/fastqc/main'
include { FASTQ_SCREEN } from './modules/fastq_screen/main'
include { SPOTYPING } from './modules/spotyping/main'
include { SPOLLINEAGES } from './modules/spollineages/main'
include { MYKROBE } from './modules/mykrobe/main'
include { FASTQTOUNMAPPEDBAM } from './modules/gatk/main'
include { BWAMEM2 } from './modules/bwa-mem2/main'
include { SAMBAMBA } from './modules/sambamba/main'
include { MERGEBAMALIGNMENT } from './modules/picard/main'
include { MARKDUPLICATES } from './modules/picard/main'
include { SORTSAM } from './modules/picard/main'
include { STATS } from './modules/samtools/main'
include { COLLECTMETRICS } from './modules/picard/main'
include { DEPTHOFCOVERAGE } from './modules/gatk/main'
include { MUTECT } from './modules/gatk/main'
include { LIFTOVERANDMERGEVCFS } from './modules/picard/main'
include { MERGEMUTECTSTATS } from './modules/gatk/main'
include { FILTERMUTECTCALLS } from './modules/gatk/main'
include { SELECTVARIANTS } from './modules/gatk/main'
include { NORM } from './modules/samtools/main'
include { ANNOTATE } from './modules/snpeff/main'
include { VARIANTSTOTABLE } from './modules/gatk/main'
include { GZANDTBI } from './modules/samtools/main'
include { CREATEREPORT } from './modules/igvreports/main'
include { PREPARE_MATRIX } from './modules/snp-dists/main'
include { MATRIX } from './modules/snp-dists/main'
include { INTERSECT_VCF } from './modules/snp-dists/main'
include { CREATEREPORT_VCF_INTERSECT } from './modules/igvreports/main'
include { SNPITRUN } from './modules/snpit/main'
include { MULTIQC } from './modules/multiqc/main'
include { XLSXMAKER } from './modules/r/main'

// Workflow declaration
workflow {
    FASTQC( read_pairs_ch )
    FASTQ_SCREEN( read_pairs_ch )
    SPOTYPING( read_pairs_ch )
    SPOLLINEAGES( SPOTYPING.out )
    MYKROBE( read_pairs_ch )
    FASTQTOUNMAPPEDBAM( read_pairs_ch )
    BWAMEM2( read_pairs_ch )
    SAMBAMBA( BWAMEM2.out )
    MERGEBAMALIGNMENT( SAMBAMBA.out.join(FASTQTOUNMAPPEDBAM.out) )
    MARKDUPLICATES( MERGEBAMALIGNMENT.out )
    SORTSAM( MARKDUPLICATES.out )
    STATS( SORTSAM.out )
    COLLECTMETRICS( SORTSAM.out )
    DEPTHOFCOVERAGE( SORTSAM.out )
    MUTECT( SORTSAM.out )
    LIFTOVERANDMERGEVCFS( MUTECT.out.vcfs )
    MERGEMUTECTSTATS( MUTECT.out.stats )
    FILTERMUTECTCALLS( LIFTOVERANDMERGEVCFS.out.join(MERGEMUTECTSTATS.out) )
    SELECTVARIANTS( FILTERMUTECTCALLS.out )
    NORM( SELECTVARIANTS.out )
    ANNOTATE( NORM.out )
    VARIANTSTOTABLE( ANNOTATE.out )
    GZANDTBI( VARIANTSTOTABLE.out.classvcfs )
    CREATEREPORT( GZANDTBI.out.join(SORTSAM.out) )
    SNPITRUN( NORM.out )
    PREPARE_MATRIX(COLLECTMETRICS.out.join(NORM.out))
    MATRIX(PREPARE_MATRIX.out.sample_id.collect().map { it.join(" ") })
    INTERSECT_VCF(MATRIX.out)
    CREATEREPORT_VCF_INTERSECT(INTERSECT_VCF.out)
    MULTIQC(
        FASTQC.out.collect(),
        FASTQ_SCREEN.out.collect(),
        STATS.out.collect(),
        COLLECTMETRICS.out.flatten().filter( ~/.*txt/ ).collect()
    )
    XLSXMAKER(
        SPOLLINEAGES.out.collect(),
        SPOTYPING.out.flatten().filter( ~/.*out/ ).collect(),
        SNPITRUN.out.collect(),MYKROBE.out.collect(),
        VARIANTSTOTABLE.out.csvs.flatten().filter( ~/.*csv/ ).collect(),
        DEPTHOFCOVERAGE.out.flatten().filter( ~/.*csv/ ).collect()
    )
}

// Workflow ending
workflow.onComplete {
	log.info ( workflow.success ? "\nDone!\n" : "\nOops .. something went wrong\n" )
}
