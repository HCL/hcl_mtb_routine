process FASTQ_SCREEN {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(reads) 

    output:
    path "fastq_screen/${sample_id}" 

    script:
    """
    mkdir -p fastq_screen/${sample_id}
    fastq_screen --conf $baseDir/modules/fastq_screen/fastq_screen.conf --outdir fastq_screen/${sample_id} ${reads[0]}
    """
}
