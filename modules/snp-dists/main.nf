process PREPARE_MATRIX {
    tag "$sample_id"
    maxForks 1

    publishDir params.matrixDir, mode:'copy', pattern: "fasta/*"
    publishDir params.matrixDir, mode:'copy', pattern: "vcf_filtered/*"

    input:
    tuple val(sample_id), path(collecthsmetrics), path(norm_vcf)

    output:
    val(sample_id), emit: sample_id
    tuple path("fasta/${sample_id}_rebuilt.fasta"), path("vcf_filtered/${sample_id}.onlySNP.vcf.gz"), path("vcf_filtered/${sample_id}.onlySNP.vcf.gz.csi"), optional: true, emit: files

    shell:
    '''
    mkdir -p fasta vcf_filtered
    cov=`grep -a1 MEAN_COVERAGE !{collecthsmetrics} | cut -f2 | tail -1 | sed "s#\\..*##g"`

    if [ $cov -gt 25 ]; then
        ln -sf !{launchDir}/!{params.outdir}/alignment/!{sample_id}*.ba* !{params.matrixDir}/alignment

        gzip !{norm_vcf} -c > !{sample_id}.norm.vcf.gz

        bcftools filter !{sample_id}.norm.vcf.gz -i "FORMAT/AF >0.80 && FORMAT/AD>=5" --SnpGap 2 --IndelGap 9 | bcftools view -v snps,mnps -M2 -Oz -o tmp_!{sample_id}.onlySNP.vcf.gz

        bcftools index tmp_!{sample_id}.onlySNP.vcf.gz

        bcftools view -R !{params.refDir}/Mycobacterium_tuberculosis_H37Rv_txt_v4_filtered_Charlotte.bed tmp_!{sample_id}.onlySNP.vcf.gz -Oz -o vcf_filtered/!{sample_id}.onlySNP.vcf.gz

        bcftools index vcf_filtered/!{sample_id}.onlySNP.vcf.gz
        echo ">!{sample_id}" > fasta/!{sample_id}_rebuilt.fasta

        bcftools consensus vcf_filtered/!{sample_id}.onlySNP.vcf.gz --fasta-ref !{params.refDir}/NC_000962.3.fa 1 -H A | grep -v '>' | tr -d '\\n' >> fasta/!{sample_id}_rebuilt.fasta ;
        echo -ne "\\n" >> fasta/!{sample_id}_rebuilt.fasta
        if ! grep -q -m 1 ">!{sample_id}" !{params.matrixDir}/MSA.fasta ; then
            cat fasta/!{sample_id}_rebuilt.fasta >> !{params.matrixDir}/MSA.fasta
        fi
    fi
    '''
}

process MATRIX {
    tag "$run"
    publishDir params.outdir, mode:'copy', pattern: "${run}_matrix_MSA.tsv"


    input:
    val(sample_id)

    output:
    tuple path("${run}_matrix_MSA.tsv"), path("${run}_matrix_MSA_molten.tsv")


    shell:

    run="$launchDir.simpleName"

    '''
    snp-dists -j !{task.cpus} -b !{params.matrixDir}/MSA.fasta > tmp_matrix_MSA.tsv
    snp-dists -j !{task.cpus} -b !{params.matrixDir}/MSA.fasta -m > tmp_matrix_MSA_molten.tsv

    head -1 tmp_matrix_MSA.tsv > tmp_!{run}_matrix_MSA.tsv

    for i in !{sample_id} ; do
        tail -n +2 tmp_matrix_MSA.tsv | grep -w $i >> tmp_!{run}_matrix_MSA.tsv || true ;
        grep -w $i tmp_matrix_MSA_molten.tsv | grep -v -w 0 | awk '{if ($3 < 21) print $0}' >> !{run}_matrix_MSA_molten.tsv || true ;
    done

    csvtk -t transpose tmp_matrix_MSA.tsv > matrix_MSA.tsv

    csvtk -t transpose tmp_!{run}_matrix_MSA.tsv > !{run}_matrix_MSA.tsv 

    cp -f matrix_MSA.tsv !{params.matrixDir}/matrix_MSA.tsv
    '''
}


process INTERSECT_VCF {
    tag "$run"
    publishDir params.matrixDir, mode:'copy', pattern: "vcf_intersect/*.onlySNP.vcf.gz*"

    input:
    tuple path(matrix), path(matrix_molten)

    output:
    tuple path("vcf_intersect/*.onlySNP.vcf.gz"), path("vcf_intersect/*.onlySNP.vcf.gz.csi"), optional: true


    shell:

    run="$launchDir.simpleName"

    '''
    mkdir -p vcf_intersect

    grep -v 0 -w !{matrix_molten} | cut -f 1-2 | while read line ; do
        id1=`echo $line | awk '{print $1}'` ;
        id2=`echo $line | awk '{print $2}'` ;
        bedtools intersect -a !{params.matrixDir}/vcf_filtered/$id1.onlySNP.vcf.gz -b !{params.matrixDir}/vcf_filtered/$id2.onlySNP.vcf.gz -v -header | bgzip > tmp_${id1}.${id2}.onlySNP.vcf.gz ;
        bcftools index -f tmp_${id1}.${id2}.onlySNP.vcf.gz ;
        bedtools intersect -b !{params.matrixDir}/vcf_filtered/$id1.onlySNP.vcf.gz -a !{params.matrixDir}/vcf_filtered/$id2.onlySNP.vcf.gz -v -header | bgzip > tmp_${id2}.${id1}.onlySNP.vcf.gz ;
        bcftools index -f tmp_${id2}.${id1}.onlySNP.vcf.gz
        bcftools merge tmp_${id1}.${id2}.onlySNP.vcf.gz tmp_${id2}.${id1}.onlySNP.vcf.gz | bgzip > vcf_intersect/${id1}.${id2}.onlySNP.vcf.gz ;
        bcftools index -f vcf_intersect/${id1}.${id2}.onlySNP.vcf.gz ;
    done
    '''
}
