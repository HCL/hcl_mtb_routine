# base image
FROM ubuntu:focal

# metadata
LABEL base.image="ubuntu:focal"
LABEL version="1"
LABEL software="snp-dists"
LABEL software.version="0.8.2"
LABEL description="Convert a FASTA alignment to SNP distance matrix"
LABEL website="https://github.com/tseemann/snp-dists"
LABEL license="https://github.com/tseemann/snp-dists/blob/master/LICENSE"
LABEL maintainer="Quentin Testard"
LABEL maintainer.email="quentin.testard@chu-lyon.fr"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  unzip \
  wget \
  git \
  gcc \
  build-essential \
  python \
  bzip2 \
  libbz2-dev \
  libncurses5-dev \
  zlib1g-dev \
  liblzma-dev \
  python3.6 \
  default-jre \
  tabix \
  && apt-get clean && apt-get autoclean && rm -rf /var/lib/apt/lists/*

RUN cd /opt && \
  samtools_version=1.15.1 && \
        wget --no-check-certificate --tries=0 -P /opt https://github.com/samtools/bcftools/releases/download/${samtools_version}/bcftools-${samtools_version}.tar.bz2 && \
        tar -jxv -C /opt -f /opt/bcftools-${samtools_version}.tar.bz2 && \
        rm -f /opt/bcftools-${samtools_version}.tar.bz2 && \
        cd /opt/bcftools-${samtools_version} && \
        ./configure && \
        make && \
        make install

RUN cd /opt && \
  snp_dists_version=0.8.2
    wget --no-check-certificate --tries=0 -P /opt https://github.com/tseemann/snp-dists/archive/v${snp_dists_version}.tar.gz && \
    tar -xzf v${snp_dists_version}.tar.gz && \
    rm v${snp_dists_version}.tar.gz && \
    cd snp-dists-${snp_dists_version} && \
    make && \
    make install

ENV PATH="${PATH}:/opt/"

ENV BEDTOOLS_INSTALL_DIR=/opt/bedtools2
ENV BEDTOOLS_VERSION=2.31.0

WORKDIR /tmp
RUN wget https://github.com/arq5x/bedtools2/releases/download/v$BEDTOOLS_VERSION/bedtools-$BEDTOOLS_VERSION.tar.gz && \
  tar -zxf bedtools-$BEDTOOLS_VERSION.tar.gz && \
  rm -f bedtools-$BEDTOOLS_VERSION.tar.gz

WORKDIR /tmp/bedtools2
RUN make && \
  mkdir --parents $BEDTOOLS_INSTALL_DIR && \
  mv ./* $BEDTOOLS_INSTALL_DIR

WORKDIR /
RUN ln -s $BEDTOOLS_INSTALL_DIR/bin/* /usr/bin/ && \
  rm -rf /tmp/bedtools2


RUN cd /opt && \
  csvtk_version=v0.27.2 && \
        wget --no-check-certificate --tries=0 -P /opt https://github.com/shenwei356/csvtk/releases/download/${csvtk_version}/csvtk_linux_amd64.tar.gz && \
        tar -zxf csvtk_linux_amd64.tar.gz && \
        rm -f csvtk_linux_amd64.tar.gz && \
        chmod 755 csvtk 
