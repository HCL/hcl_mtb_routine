process SAMBAMBA {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(bam), path(bamShifted) 

    output:
    tuple val(sample_id), path("${sample_id}.sort.bam"), path("${sample_id}.sort.shifted.bam")

    script:
    """
    sambamba -q view -t $task.cpus -S -f bam -l 0 ${bam} | \
    sambamba -q sort -t $task.cpus -m 6G /dev/stdin -o ${sample_id}.sort.bam --tmpdir="tmp/"

    sambamba -q view -t $task.cpus -S -f bam -l 0 ${bamShifted} | \
    sambamba -q sort -t $task.cpus -m 6G /dev/stdin -o ${sample_id}.sort.shifted.bam --tmpdir="tmp/"
    """
}
