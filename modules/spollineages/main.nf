process SPOLLINEAGES {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(spotyping)

    output:
    path "spollineages/${sample_id}.csv"

    script:
    """
    mkdir -p spollineages/${sample_id}
    
    cut -f2 ${spotyping} | awk -v id_ok=${sample_id} '{print id_ok";"\$0}' > spollineages/${sample_id}.input

    cp -r /opt/SpolLineages/Decision_Tree spollineages/
    cp -r /opt/SpolLineages/Binary_Mask2 spollineages/

    java -jar /opt/SpolLineages/spollineages.jar \
        -i spollineages/${sample_id}.input \
        -o spollineages/${sample_id}.csv \
        -D \
        -pDT spollineages/Decision_Tree/ \
        -E \
        -pEA spollineages/Binary_Mask2/ \
        -a    
    """
}
