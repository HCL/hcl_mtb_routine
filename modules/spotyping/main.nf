process SPOTYPING {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(reads) 

    output:
    tuple val(sample_id), path("spotyping/${sample_id}.spo.out")

    script:
    """
    mkdir -p spotyping
    python2.7 /opt/SpoTyping-2.0/SpoTyping-v2.0-commandLine/SpoTyping.py \
        --noQuery \
        -m 3 \
        ${reads[0]} \
        ${reads[1]} \
        -o spotyping/${sample_id}.spo.out
    """
}
