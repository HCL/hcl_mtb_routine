process ANNOTATE {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(vcf)

    output:
    tuple val(sample_id), path("annotation/${sample_id}.who.vcf")

    script:
    """
    mkdir -p annotation
    java -jar /opt/SnpSift.jar \
        annotate \
            -c /opt/snpEff/snpEff.config \
            -v ${params.refDir}/WHO-UCN-GTB-PCI-2021.7-eng.norm.vcf \
            ${vcf} \
            > annotation/${sample_id}.who.vcf
    """
}
