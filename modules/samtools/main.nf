process STATS {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id),path(bam),path(bai),path(bamShifted),path(baiShifted)

    output:
    path "samtools_stats/${sample_id}.txt"

    script:
    """
    mkdir -p samtools_stats
    samtools stats ${bam} > samtools_stats/${sample_id}.txt
    """
}

process NORM {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(vcf)

    output:
    tuple val(sample_id), path("calling/${sample_id}.norm.vcf")

    script:
    """
    mkdir -p calling
    bcftools norm -d all -o calling/${sample_id}.nodups.vcf ${vcf}
    bcftools norm -m-both -o calling/${sample_id}.split.vcf calling/${sample_id}.nodups.vcf
    bcftools norm -f ${params.refDir}/NC_000962.3.fa -o calling/${sample_id}.norm.vcf calling/${sample_id}.split.vcf
    """
}

process GZANDTBI {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(vcf)

    output:
    tuple val(sample_id), path("${sample_id}.vcf.gz"), path("${sample_id}.vcf.gz.tbi")

    script:
    """
    bgzip -c ${vcf} > ${sample_id}.vcf.gz
    tabix -p vcf ${sample_id}.vcf.gz
    """
}
