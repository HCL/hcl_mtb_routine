process MERGEBAMALIGNMENT {
    tag "$sample_id"

    input:
    tuple val(sample_id),path(bam),path(bamShifted),path(bamUnmapped)

    output:
    tuple val(sample_id), path("${sample_id}.mba.bam"), path("${sample_id}.shifted.mba.bam") 

    script:
    """
    mkdir -p tmp/
    java -Xms3000m -jar /opt/picard.jar \
            MergeBamAlignment \
            TMP_DIR=tmp/ \
            VALIDATION_STRINGENCY=SILENT \
            EXPECTED_ORIENTATIONS=FR \
            ATTRIBUTES_TO_RETAIN=X0 \
            ATTRIBUTES_TO_REMOVE=NM \
            ATTRIBUTES_TO_REMOVE=MD \
            ALIGNED_BAM=${bam} \
            UNMAPPED_BAM=${bamUnmapped} \
            OUTPUT=${sample_id}.mba.bam \
            REFERENCE_SEQUENCE=${params.refDir}/NC_000962.3.fa \
            PAIRED_RUN=true \
            SORT_ORDER="unsorted" \
            IS_BISULFITE_SEQUENCE=false \
            ALIGNED_READS_ONLY=false \
            CLIP_ADAPTERS=false \
            MAX_RECORDS_IN_RAM=2000000 \
            ADD_MATE_CIGAR=true \
            MAX_INSERTIONS_OR_DELETIONS=-1 \
            PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
            UNMAPPED_READ_STRATEGY=COPY_TO_TAG \
            ALIGNER_PROPER_PAIR_FLAGS=true \
            UNMAP_CONTAMINANT_READS=true \
            ADD_PG_TAG_TO_READS=false

    java -Xms3000m -jar /opt/picard.jar \
            MergeBamAlignment \
            TMP_DIR=tmp/ \
            VALIDATION_STRINGENCY=SILENT \
            EXPECTED_ORIENTATIONS=FR \
            ATTRIBUTES_TO_RETAIN=X0 \
            ATTRIBUTES_TO_REMOVE=NM \
            ATTRIBUTES_TO_REMOVE=MD \
            ALIGNED_BAM=${bamShifted} \
            UNMAPPED_BAM=${bamUnmapped} \
            OUTPUT=${sample_id}.shifted.mba.bam \
            REFERENCE_SEQUENCE=${params.refDir}/NC_000962.3.shifted.fa \
            PAIRED_RUN=true \
            SORT_ORDER="unsorted" \
            IS_BISULFITE_SEQUENCE=false \
            ALIGNED_READS_ONLY=false \
            CLIP_ADAPTERS=false \
            MAX_RECORDS_IN_RAM=2000000 \
            ADD_MATE_CIGAR=true \
            MAX_INSERTIONS_OR_DELETIONS=-1 \
            PRIMARY_ALIGNMENT_STRATEGY=MostDistant \
            UNMAPPED_READ_STRATEGY=COPY_TO_TAG \
            ALIGNER_PROPER_PAIR_FLAGS=true \
            UNMAP_CONTAMINANT_READS=true \
            ADD_PG_TAG_TO_READS=false
    """
}

process MARKDUPLICATES {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(bam), path(bamShifted)

    output:
    tuple val(sample_id), path("${sample_id}.markdup.bam"), path("${sample_id}.shifted.markdup.bam") 

    script:
    """
    mkdir -p tmp/

   java -Xms4000m -jar /opt/picard.jar \
            MarkDuplicates \
            TMP_DIR=tmp/ \
            INPUT=${bam} \
            OUTPUT=${sample_id}.markdup.bam \
            METRICS_FILE=${sample_id}.markdup.metrics.txt \
            VALIDATION_STRINGENCY=SILENT \
            OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
            ASSUME_SORT_ORDER="queryname" \
            CLEAR_DT="false" \
            ADD_PG_TAG_TO_READS=false

    java -Xms4000m -jar /opt/picard.jar \
            MarkDuplicates \
            TMP_DIR=tmp/ \
            INPUT=${bamShifted} \
            OUTPUT=${sample_id}.shifted.markdup.bam \
            METRICS_FILE=${sample_id}.shifted.markdup.metrics.txt \
            VALIDATION_STRINGENCY=SILENT \
            OPTICAL_DUPLICATE_PIXEL_DISTANCE=2500 \
            ASSUME_SORT_ORDER="queryname" \
            CLEAR_DT="false" \
            ADD_PG_TAG_TO_READS=false
    """
}

process SORTSAM {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy', pattern: "alignment/${sample_id}.bam"
    publishDir params.outdir, mode:'copy', pattern: "alignment/${sample_id}.bai"


    input:
    tuple val(sample_id),path(bam),path(bamShifted)

    output:
    tuple val(sample_id), path("alignment/${sample_id}.bam"), path("alignment/${sample_id}.bai"), path("alignment/${sample_id}.shifted.bam"), path("alignment/${sample_id}.shifted.bai")

    script:
    """
    mkdir -p alignment/
 
    java -Xms4000m -jar /opt/picard.jar \
            SortSam \
            TMP_DIR=. \
            INPUT=${bam} \
            OUTPUT=alignment/${sample_id}.bam \
            SORT_ORDER="coordinate" \
            CREATE_INDEX=true \
            MAX_RECORDS_IN_RAM=300000

    java -Xms4000m -jar /opt/picard.jar \
            SortSam \
            TMP_DIR=tmp/ \
            INPUT=${bamShifted} \
            OUTPUT=alignment/${sample_id}.shifted.bam \
            SORT_ORDER="coordinate" \
            CREATE_INDEX=true \
            MAX_RECORDS_IN_RAM=300000
    """
}

process COLLECTMETRICS {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id),path(bam),path(bai),path(bamShifted),path(baiShifted)

    output:
    tuple val(sample_id), path ("picard_metrics/${sample_id}.txt")

    script:
    """
    mkdir -p picard_metrics
    if [[ ${sample_id} =~ "Amplicon" ]]; then
        java -jar /opt/picard.jar CollectHsMetrics \
                --INPUT ${bam} \
                --OUTPUT picard_metrics/${sample_id}.txt \
                --TARGET_INTERVALS ${params.refDir}/CRISPR_locus.interval_list \
                --BAIT_INTERVALS ${params.refDir}/CRISPR_locus.interval_list
    else
        java -jar /opt/picard.jar CollectWgsMetrics \
                --INPUT ${bam} \
                --OUTPUT picard_metrics/${sample_id}.txt \
                --REFERENCE_SEQUENCE ${params.refDir}/NC_000962.3.fa
    fi
    """
}

process LIFTOVERANDMERGEVCFS {
    tag "$sample_id"

    input:
    tuple val(sample_id),path(vcf),path(vcfShifted)

    output:
    tuple val(sample_id), path("${sample_id}.final.vcf") 

    script:
    """
    java -jar /opt/picard.jar \
            LiftoverVcf \
            I=${vcfShifted} \
            O=${sample_id}.shifted_back.vcf \
            R=${params.refDir}/NC_000962.3.fa \
            CHAIN=${params.refDir}/NC_000962.3.shiftback.chain \
            REJECT=${sample_id}.rejected.vcf

    java -jar /opt/picard.jar \
            MergeVcfs \
            I=${sample_id}.shifted_back.vcf \
            I=${vcf} \
            O=${sample_id}.final.vcf
    """
}
