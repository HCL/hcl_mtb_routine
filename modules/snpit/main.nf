process SNPITRUN {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'
    
    input:
    tuple val(sample_id), path(vcf)

    output:
    path "snpit/${sample_id}.snpit.out" 

    script:
    """
    mkdir -p snpit
    snpit-run.py --input ${vcf} | grep vcf > snpit/${sample_id}.snpit.out
    """
}
