process CREATEREPORT {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(vcfgz), path(vcfgztbi), path(bam), path(bai), path(bamShifted), path(baiShifted)

    output:
    path "igv_report/${sample_id}.html"

    script:
    """
    mkdir -p igv_report
    create_report ${vcfgz} \
        ${params.refDir}/NC_000962.3.fa \
        --tracks ${vcfgz} \
        ${bam} \
        --output igv_report/${sample_id}.html \
        --flanking 1000 \
        --info-columns Gene Variant LocusTag RIF_Conf_Grade INH_Conf_Grade EMB_Conf_Grade PZA_Conf_Grade LEV_Conf_Grade LZD_Conf_Grade MXF_Conf_Grade BDQ_Conf_Grade CFZ_Conf_Grade DLM_Conf_Grade AMI_Conf_Grade STM_Conf_Grade ETH_Conf_Grade KAN_Conf_Grade CAP_Conf_Grade
    """
}

process CREATEREPORT_VCF_INTERSECT {
    tag "$run"
    publishDir params.matrixDir, mode:'copy', pattern: 'matrix_plot/*.html'
    publishDir params.outdir, mode:'copy', pattern: 'matrix_plot/*.html'


    input:
    tuple path(intersected_vcf), path(intersected_index)

    output:
    path("matrix_plot/*.html"), optional: true


    shell:

    run="$launchDir.simpleName"
    
    '''
    mkdir -p matrix_plot

    for i in !{intersected_vcf} ;
        do id1=`echo $i | cut -f1 -d"." | sed "s/.*\\///g"` ;
        id2=`echo $i | cut -f2 -d"."` ;
        if [ ! -f "!{params.matrixDir}/matrix_plot/${id2}_${id1}.html" ];
            then create_report !{params.matrixDir}/vcf_intersect/${id1}.${id2}.onlySNP.vcf.gz \
            !{params.refDir}/NC_000962.3.fa \
            --tracks !{params.matrixDir}/vcf_intersect/${id1}.${id2}.onlySNP.vcf.gz \
            !{params.matrixDir}/alignment/$id1.bam !{params.matrixDir}/alignment/$id2.bam \
            --output matrix_plot/${id1}_${id2}.html \
            --flanking 1000 \
            --samples $id1 $id2 \
            --sample-columns AD AF ;
         fi ;
    done
    '''
}
