process FASTQTOUNMAPPEDBAM {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(reads) 

    output:
    tuple val(sample_id), path("${sample_id}.unmapped.bam")

    script:
    """
    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar FastqToSam \
        --FASTQ ${reads[0]} \
        --FASTQ2 ${reads[1]} \
	--TMP_DIR . \
        --OUTPUT ${sample_id}.unmapped.bam \
        --READ_GROUP_NAME ${sample_id} \
        --SAMPLE_NAME ${sample_id} \
        --PLATFORM Illumina
    """
}

process DEPTHOFCOVERAGE {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id),path(bam),path(bai),path(bamShifted),path(baiShifted)

    output:
    tuple val(sample_id), path("coverage/${sample_id}.sample_interval_summary.csv"), emit: coverage

    script:
    """
    mkdir -p coverage

    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar DepthOfCoverage \
        --tmp-dir . \
	-R ${params.refDir}/NC_000962.3.fa \
        -I ${bam} \
        -O ${sample_id} \
        --intervals ${params.refDir}/H37Rv.bed \
        --summary-coverage-threshold 30

    echo -ne "${sample_id}," > coverage/${sample_id}.sample_interval_summary.csv
    cut -f 3,9 -d "," ${sample_id}.sample_interval_summary | tail -31 | sed "s/,/X,/g" | tr "\\n" "," | sed "s/,\$/\\n/g" >> coverage/${sample_id}.sample_interval_summary.csv  
    """
}

process MUTECT {
    tag "$sample_id"

    input:
    tuple val(sample_id),path(bam),path(bai),path(bamShifted),path(baiShifted)

    output:
    tuple val(sample_id), path("${sample_id}.vcf"), path("${sample_id}.shifted.vcf"), emit: vcfs
    tuple val(sample_id), path("${sample_id}.vcf.stats"), path("${sample_id}.shifted.vcf.stats"), emit: stats

    script:
    """
    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar Mutect2 \
        -R ${params.refDir}/NC_000962.3.fa \
        -I ${bam} \
        -O ${sample_id}.vcf \
	--pileup-detection true \
        --annotation StrandBiasBySample \
        --num-matching-bases-in-dangling-end-to-recover 1 \
        --max-reads-per-alignment-start 75

    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar Mutect2 \
        -R ${params.refDir}/NC_000962.3.shifted.fa \
        -I ${bamShifted} \
        -O ${sample_id}.shifted.vcf \
	--pileup-detection true \
        --annotation StrandBiasBySample \
        --num-matching-bases-in-dangling-end-to-recover 1 \
        --max-reads-per-alignment-start 75
    """
}

process MERGEMUTECTSTATS {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(vcfStats), path(vcfShiftedStats) 

    output:
    tuple val(sample_id), path("${sample_id}.combined.stats")

    script:
    """
    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar \
        MergeMutectStats \
            --stats ${vcfShiftedStats} \
            --stats ${vcfStats} \
            -O ${sample_id}.combined.stats
    """
}

process FILTERMUTECTCALLS {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(vcf), path(stats)

    output:
    tuple val(sample_id), path("${sample_id}.filter.vcf")

    script:
    """
    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar \
        FilterMutectCalls \
            --tmp-dir . \
            -V ${vcf} \
            -R ${params.refDir}/NC_000962.3.fa \
            -O ${sample_id}.filter.vcf \
            --stats ${stats} \
            --microbial-mode
    """
}

process SELECTVARIANTS {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(vcf)

    output:
    tuple val(sample_id), path("${sample_id}.clean.vcf")

    script:
    """
    java -jar /opt/gatk-${params.versions.gatk}/gatk-package-${params.versions.gatk}-local.jar \
        SelectVariants \
            --exclude-filtered \
            --tmp-dir . \
            -V ${vcf} \
            -R ${params.refDir}/NC_000962.3.fa \
            -O ${sample_id}.clean.vcf
    """
}

process VARIANTSTOTABLE {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy', pattern: "annotation/*"

    input:
    tuple val(sample_id), path(vcf)

    output:
    tuple val(sample_id), path("annotation/${sample_id}.csv"), emit: csvs
    tuple val(sample_id), path("${sample_id}.class.vcf"), emit: classvcfs

    shell:

    '''
    mkdir -p annotation
    grep "^#" !{vcf} > !{sample_id}.class.vcf
    { grep "[12345])_" !{vcf} >> !{sample_id}.class.vcf || true; }

    java -jar /opt/gatk-!{params.versions.gatk}/gatk-package-!{params.versions.gatk}-local.jar VariantsToTable \
        --tmp-dir . \
        -V !{sample_id}.class.vcf \
        -O !{sample_id}.tmp.csv \
        --show-filtered \
        -F CHROM \
        -F POS \
        -F REF \
        -F ALT \
        -F Gene \
        -F Variant \
        -F AMI_Conf_Grade \
        -F BDQ_Conf_Grade \
        -F CAP_Conf_Grade \
        -F CFZ_Conf_Grade \
        -F DLM_Conf_Grade \
        -F EMB_Conf_Grade \
        -F ETH_Conf_Grade \
        -F INH_Conf_Grade \
        -F KAN_Conf_Grade \
        -F LEV_Conf_Grade \
        -F LZD_Conf_Grade \
        -F MXF_Conf_Grade \
        -F PZA_Conf_Grade \
        -F RIF_Conf_Grade \
        -F STM_Conf_Grade \
        -GF DP \
	-GF AD


    echo -ne "Sample\\t" > annotation/!{sample_id}.csv

    sed "s/STM_Conf_Grade\t.*DP/STM_Conf_Grade\tDP/g" !{sample_id}.tmp.csv | sed "s/DP\t.*AD/DP\tAD/g" | \
    awk -F "\t" 'BEGIN{OFS="\t";} {print $1,$2,$3,$4,$5,$6,$22,$23,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21}' > !{sample_id}.tmp.tmp.csv

    head -n 1 !{sample_id}.tmp.tmp.csv >> annotation/!{sample_id}.csv
    sed 1d !{sample_id}.tmp.tmp.csv | awk -v id=!{sample_id} '{print id"\\t"\$0}' >> annotation/!{sample_id}.csv
    '''
}
