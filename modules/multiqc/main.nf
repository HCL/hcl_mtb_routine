process MULTIQC {
    tag "$run"
    publishDir params.outdir, mode:'copy'

    input:
    path ('fastqc/*')
    path ('fastq_screen/*')
    path ('samtools_stats/*')
    path ('picard_metrics/*')
    
    output:
    path "multiqc/${run}_multiqc_report.html"
    path "multiqc/Couverture_mqc.yml"

    script:
    run="$launchDir.simpleName"

    """
    mkdir -p multiqc
    echo "data:" > multiqc/Couverture_mqc.yml
    
    { grep -s ^BAIT_SET picard_metrics/*.txt -A 1 | cut -f1,34 | grep locus | rev | cut -f1 -d"/" | rev | sed -re 's@^@    @' | sed -re 's@.txt-CRISPR_locus\\t@:\\n        Couv: @' >> multiqc/Couverture_mqc.yml || true; }
    
    { grep -s ^GENOME_TERRITORY picard_metrics/*.txt -A 1 | cut -f1,2 | grep txt- | rev | cut -f1 -d"/" | rev | sed -re 's@^@    @' | sed -re 's@.txt-4411532\\t@:\\n        Couv: @' >> multiqc/Couverture_mqc.yml || true; }

    multiqc -i $run -n ${run}_multiqc_report -o multiqc -c $projectDir/modules/multiqc/multiqc_config.yaml . --force
    """
}
