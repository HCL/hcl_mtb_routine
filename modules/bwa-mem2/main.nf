process BWAMEM2 {
    tag "$sample_id"

    input:
    tuple val(sample_id), path(reads) 

    output:
    tuple val(sample_id), path("${sample_id}.sam"), path("${sample_id}.shifted.sam")

    script:
    """
    bwa-mem2 mem -t $task.cpus ${params.refDir}/NC_000962.3.fa ${reads[0]} ${reads[1]} > ${sample_id}.sam
    bwa-mem2 mem -t $task.cpus ${params.refDir}/NC_000962.3.shifted.fa ${reads[0]} ${reads[1]} > ${sample_id}.shifted.sam
    """
}
