process FASTQC {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(reads) 

    output:
    path "fastqc/${sample_id}" 

    script:
    """
    mkdir -p fastqc/${sample_id}
    fastqc -t $task.cpus -o fastqc/${sample_id} -f fastq -q ${reads[0]}
    """
}
