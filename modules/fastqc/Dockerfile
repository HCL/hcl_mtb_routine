# base image
FROM ubuntu:focal

# metadata
LABEL base.image="ubuntu:focal"
LABEL version="1"
LABEL software="FastQC"
LABEL software.version="0.11.9"
LABEL description="A quality control analysis tool for high throughput sequencing data"
LABEL website="https://www.bioinformatics.babraham.ac.uk/projects/fastqc/"
LABEL license="https://github.com/s-andrews/FastQC/blob/master/LICENSE.txt"
LABEL maintainer="Maxime Vallée"
LABEL maintainer.email="maxime.vallee@chu-lyon.fr"

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  unzip \
  wget \
  perl \
  default-jre \
  && apt-get clean && apt-get autoclean && rm -rf /var/lib/apt/lists/*

RUN cd /opt && \
    wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip && \
    unzip fastqc_v0.11.9.zip && \
    rm fastqc_v0.11.9.zip && \
    chmod +x FastQC/fastqc

ENV PATH="${PATH}:/opt/FastQC/"
