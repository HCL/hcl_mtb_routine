process MYKROBE {
    tag "$sample_id"
    publishDir params.outdir, mode:'copy'

    input:
    tuple val(sample_id), path(reads) 

    output:
    path "mykrobe/${sample_id}.csv" 

    script:
    """
    mkdir -p mykrobe/

    mykrobe predict -t $task.cpus \
	--tmp tmp/ \
        -s ${sample_id} \
        -S tb \
        -o mykrobe/${sample_id}.csv \
        -i ${reads[0]} ${reads[1]}
    """
}
