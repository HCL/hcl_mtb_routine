suppressPackageStartupMessages({
  library(xlsx)
  library(dplyr)
})

run = commandArgs(trailingOnly=T)[2]

setwd(commandArgs(trailingOnly=T)[1])

tab=read.csv2("report.csv")
gatkWho=read.csv2("GatkWho.csv")
coverage=read.csv2("coverage.csv")


grading <- function(Conf_Grade) {
  class1 = '1)_Assoc_w_R' %in% unlist(Conf_Grade)
  class2 = '2)_Assoc_w_R_-_Interim' %in% unlist(Conf_Grade)
  class3 = '3)_Uncertain_significance' %in% unlist(Conf_Grade)
  class4 = '4)_Not_assoc_w_R_-_Interim' %in% unlist(Conf_Grade)
  class5 = '5)_Not_assoc_w_R' %in% unlist(Conf_Grade)

  pattern = ""
  if(class1 || class2) {
    pattern = "R"
  } else if(class3) {
    pattern = "?"
  } else if(class4 || class5) {
    pattern = "S"
  } else {
    pattern = "S"
  }

  return(pattern)
}

simpleWho = gatkWho %>%
  mutate(PZA_Conf_Grade = as.factor(PZA_Conf_Grade)) %>%
  mutate(PZA_Conf_Grade = case_when(Gene == 'pncA' ~ PZA_Conf_Grade, TRUE ~ as.factor(NA))) %>%
  mutate(INH_Conf_Grade = as.factor(INH_Conf_Grade)) %>%
  mutate(INH_Conf_Grade = case_when(Gene != 'Rv2752c' ~ INH_Conf_Grade, TRUE ~ as.factor(NA))) %>%
  mutate(RIF_Conf_Grade = as.factor(RIF_Conf_Grade)) %>%
  mutate(RIF_Conf_Grade = case_when(Gene != 'Rv2752c' ~ RIF_Conf_Grade, TRUE ~ as.factor(NA))) %>%
  group_by(Sample) %>%
  summarise(
    OMS_AMI=grading(AMI_Conf_Grade),
    OMS_BDQ=grading(BDQ_Conf_Grade),
    OMS_CAP=grading(CAP_Conf_Grade),
    OMS_CFZ=grading(CFZ_Conf_Grade),
    OMS_DLM=grading(DLM_Conf_Grade),
    OMS_EMB=grading(EMB_Conf_Grade),
    OMS_ETH=grading(ETH_Conf_Grade),
    OMS_INH=grading(INH_Conf_Grade),
    OMS_KAN=grading(KAN_Conf_Grade),
    OMS_LEV=grading(LEV_Conf_Grade),
    OMS_LZD=grading(LZD_Conf_Grade),
    OMS_MXF=grading(MXF_Conf_Grade),
    OMS_PZA=grading(PZA_Conf_Grade),
    OMS_RIF=grading(RIF_Conf_Grade),
    OMS_STM=grading(STM_Conf_Grade)
  )

simpleTab = tab %>%
  full_join(simpleWho) %>%
  select(Sample,Spoligo.Binary,Spoligo.Octal,Curated.lineage..SITVIT2.,SNP.based.lineage,SIT,SNP.IT.Species,SNP.IT.Percentage,Mykrobe_Ethambutol,OMS_EMB,Mykrobe_Isoniazid,OMS_INH,Mykrobe_Pyrazinamide,OMS_PZA,Mykrobe_Rifampicin,OMS_RIF,Mykrobe_Streptomycin,OMS_STM)

write.xlsx(simpleTab, file=paste0(run,".xlsx"), sheetName="resume", row.names=FALSE)
write.xlsx(coverage, file=paste0(run,".xlsx"), sheetName="coverage", append=TRUE, row.names=FALSE)
write.xlsx(tab, file=paste0(run,".xlsx"), sheetName="complet", append=TRUE, row.names=FALSE)
write.xlsx(gatkWho, file=paste0(run,".xlsx"), sheetName="OMS_resistance", append=TRUE, row.names=FALSE)
