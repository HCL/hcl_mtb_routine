process XLSXMAKER {
    tag "$run"
    publishDir params.outdir, mode:'copy'

    input:
    path ('spollineages/*')
    path ('spotyping/*')
    path ('snpit/*')
    path ('mykrobe/*')
    path ('annotation/*')
    path ('coverage/*')

    output:
    path "${run}.xlsx"

    script:
    run="$launchDir.simpleName"

    """
    for id in \$(ls -d mykrobe/*.csv | cut -f2 -d"/" | rev | cut -f2 -d"." | rev); do
        if [ "\$(wc -l mykrobe/\${id}.csv | awk '{print \$1}')" == 2 ];  then
            echo "NA;NA;NA;NA;NA;NA;NA;NA;NA;NA;NA" > mykrobe/\${id}.tr.csv
        else
            ruby -rcsv -e 'puts CSV.parse(STDIN).transpose.map &:to_csv' < mykrobe/\${id}.csv | grep ^[dsu][rus] | cut -f2- -d"," | tr ',' ';' > mykrobe/\${id}.tr.csv
        fi
    done


    echo "Sample;Spoligo Binary;Spoligo Octal;MIRU-VNTR;Lineage (binary rules);SubLineage (binary rules);Other candidate lineages;Lineage (decision tree);Lineage (evolutionary algorithm);Curated lineage (SITVIT2);SNP-based lineage;MIRU-VNTR based lineage (RuleTB);SIT;Country Distribution (SITVIT2);SNP-IT Species;SNP-IT Lineage;SNP-IT Name;SNP-IT Percentage;Mykrobe_Amikacin;Mykrobe_Capreomycin;Mykrobe_Ciprofloxacin;Mykrobe_Ethambutol;Mykrobe_Isoniazid;Mykrobe_Kanamycin;Mykrobe_Moxifloxacin;Mykrobe_Ofloxacin;Mykrobe_Pyrazinamide;Mykrobe_Rifampicin;Mykrobe_Streptomycin" > report.csv

    paste -d ';' <(grep --no-filename -v StrainID spollineages/*.csv | cut -f1 -d';') <(cut -f2- spotyping/*out | awk '{print "'\\''"\$1";'\\''"\$2}') <(grep --no-filename -v StrainID spollineages/*.csv | cut -f4- -d';') <(cut -f2- snpit/*out | tr '\\\t' ';') <(grep --no-filename -v Streptomycin mykrobe/*.tr.csv) >> report.csv

    head -n 1 \$(ls annotation/*.csv | head -n 1) | tr '\\\t' ';' > GatkWho.csv
    grep --no-filename -v Conf_Grade annotation/*csv | tr '\\\t' ';' | sed "s/,/\\//g" >> GatkWho.csv

    cat ${params.refDir}/H37Rv.header coverage/*interval_summary.csv > coverage.csv

    sed -i -e "s/,/;/g" coverage.csv

    Rscript $projectDir/modules/r/XLSX_maker.r \$PWD $run
    """
}
