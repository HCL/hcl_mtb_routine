[[_TOC_]]

# HCL MTB routine

**Genestet C.#**<sup>1, 2</sup>, **Testard Q.#**<sup>3</sup>, Vallée M.<sup>3</sup>, Hodille E.<sup>2</sup>, Bardel C.<sup>3, 4</sup>, Dumitrescu O.<sup>1, 2</sup> # co-first

<sup>1</sup> CIRI - Centre International de Recherche en Infectiologie, Inserm U1111, CNRS UMR5308, Ecole Normale Supérieure de Lyon, Université Claude Bernard Lyon 1

<sup>2</sup> Institut des Agents Infectieux, Laboratoire de Bactériologie, Hospices Civils de Lyon

<sup>3</sup> Plateforme NGS-HCL, Cellule bioinformatique, Hospices Civils de Lyon

<sup>4</sup> Laboratoire de Biométrie et Biologie Evolutive UMR5558, équipe biostatistique-santé, CNRS, Université Claude Bernard Lyon 1


# Introduction

HCL MTB routine is an Hopices Civils de Lyon pipeline, with the purpose to routinely analyze whole genome sequencing data from *Mycobacterium tuberculosis*.

![](HCL_MTB_routine.png)

Ce pipeline répond aux besoins :
This pipeline aims to :
- perform extensive quality control (post sequecing, probable contamination while flowcell sharing, mapping and depth metrics)
- spoligotype (both on FASTQ and on variant calling)
- identify drug resistance (both on spoligotype and on variant calling)
- generate and send automatically final deliverables (HTML report and XLSX file)

# Prerequisites

This pipeline is written in [Nextflow](https://www.nextflow.io) [DSL2](https://www.nextflow.io/docs/latest/dsl2.html) workflow langage, a recent [installation](https://www.nextflow.io/index.html#GetStarted) (`>=20.07.1`) is mandatory.


To be free from installation issues, and to save all used softwares' versions, each pipeline process is linked to an image hosted on [Dockerhub](https://hub.docker.com/u/valleema). A [Singularity](https://sylabs.io/guides/3.0/user-guide/installation.html#distribution-packages-of-singularity) (version 3+) install is mandatory as well to run this pipeline.

For contamination detection, mapping, and variant calling, reference genomes should be avaiable on your filesystem.
Both the `$refDir` parameter of [nextflow.config](./nextflow.config) and the `Fastq Screen` [configuration](./modules/fastq_screen/fastq_screen.conf) should be edited as needed.

# Usage

For an example analysis, named here `Mycobacterium-20110412` :

```sh
# Pulling the code
git clone https://gitlab.inria.fr/NGS/hcl_mtb_routine.git

# Folder creation
folder=Mycobacterium-20110412
mkdir -p ${folder}/input
cd ${folder}

# Concatenate FASTQ.GZ per lane, usually looped for all ${sample}s, for both R1 and R2
zcat /path/to/${folder}/FASTQ/${sample}*_R1_001.fastq.gz > input/${sample}_R1.fastq
zcat /path/to/${folder}/FASTQ/${sample}*_R2_001.fastq.gz > input/${sample}_R2.fastq

# Command-line launch, directly in the ${folder}
nextflow run /path/to/cloned/hcl_mtb_routine/main.nf
```

# Outputs

All output are copied to `results/` folder.

# Probable issues

In case of many parallel queries, it is possible to experience Dockerhub image pulling limitation. A workaround would be to download manually images, using this command to generate all the `singularity pull` to be manually run:
```sh
grep "versions " hcl_mtb_routine/nextflow.config -A 15 | sed 1d | tr -d "'" | awk '{print "singularity pull --name valleema-"$1"-"$3".img docker://valleema/"$1":"$3}'
```
