#!/bin/bash
#set -e
set -x

# Cron pour le lancement automatique de la pipeline

# Pour le log de cron
date

# Surveillance du dossier Run (à partir du )
runsPrets=$(ls -tr --time-style=long-iso /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/*/MultiQC/multiqc_data/multiqc.log | cut -f10 -d'/' | sort)

# Listing des runs faits
runFaits=$(ls /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/*/*_multiqc_report.html /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/*/results/multiqc/*_multiqc_report.html 2>/dev/null | cut -f8 -d'/' | sort)

# Dossier à faire
aFaire=$(join <(echo "$runsPrets") <(echo "$runFaits") -v 1)

for folder in $aFaire; do
    echo "traitement de $folder"
    date
    for sample in $(ls /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/*.fastq.gz | cut -f12 -d"/" | rev | cut -f4- -d"_" | rev | sort -u); do

        # S'il y a Amplicon dans le nom, on SpoMutScan et SpolLineage
        if [ $(echo $sample | grep -c Amplicon) -eq 1 ]; then

        	mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon

        	# Faux MultiQC pour calmer le cron
        	mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/multiqc
            	touch /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/multiqc/${folder}_multiqc_report.html

            nbReads=$(zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*gz | grep -c "^@")

            if [ "$nbReads" -gt 99999 ]; then
                # Pour ne pas melanger les inputs
                mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon

                # Creation des FASTQ
                zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*_R1_001.fastq.gz > /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon/${sample}_R1.fastq
                zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*_R2_001.fastq.gz > /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon/${sample}_R2.fastq

                # SpoMutScan
                if [ ! -f /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/spomutscan/${sample}.mutscan.spo.out ]; then
                    mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/spomutscan

                    cd /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/spomutscan

                    singularity exec -B /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/:/srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/ /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/Singularity/SpoMutScan_20220427-150255.sif \
                        /opt/SpoMutScan.sh \
                        -1 /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon/${sample}_R1.fastq \
                        -2 /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input_amplicon/${sample}_R2.fastq \
                        -t /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/tmp \
                        -n 5
                fi

                # SpolLineages
                mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/spollineages

                cd /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results

                cut -f2 spomutscan/${sample}.mutscan.spo.out | awk -v id_ok=${sample} '{print id_ok";"$0}' > spollineages/${sample}.input

                singularity exec -B /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/:/srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/ /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/Singularity/spollineages-adb3b1f.img cp -r /opt/SpolLineages/Decision_Tree spollineages/

                singularity exec -B /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/:/srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/ /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/Singularity/spollineages-adb3b1f.img cp -r /opt/SpolLineages/Binary_Mask2 spollineages/

                singularity exec -B /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/:/srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/ /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/Singularity/spollineages-adb3b1f.img \
                    java -jar /opt/SpolLineages/spollineages.jar \
                        -i spollineages/${sample}.input \
                        -o spollineages/${sample}.csv \
                        -D \
                        -pDT spollineages/Decision_Tree/ \
                        -E \
                        -pEA spollineages/Binary_Mask2/ \
                        -a >> /dev/null 2>&1

                rm spollineages/${sample}.input
                rm -r spollineages/Binary_Mask2
                rm -r spollineages/Decision_Tree
                continue
            else
                warningCur="Avertissement : l'echantillon $sample ne contient pas assez de reads ($nbReads) et ne sera pas analyse.\n"
                warning=$warning$warningCur
                continue
            fi
        fi

        nbReads=$(zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*gz | head -n 4000 | grep -c "^@")

	# Faux MultiQC pour calmer le cron
	mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/multiqc
	touch /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/results/multiqc/${folder}_multiqc_report.html

        if [ "$nbReads" -eq 1000 ]; then
            mkdir -p /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input
            if [ ! -f "/srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input/${sample}_R1.fastq" ]; then
                zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*_R1_001.fastq.gz > /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input/${sample}_R1.fastq
                zcat /srv/nfs/ngs-stockage/NGS_Archives/NGS-DIAG/Demultiplexages/Mycobacterium/*/${folder}/FASTQ/${sample}*_R2_001.fastq.gz > /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/input/${sample}_R2.fastq
            fi
        else
            warningCur="Avertissement : l'echantillon $sample ne contient pas assez de reads ($nbReads) et ne sera pas analyse.\n"
            warning=$warning$warningCur
        fi
    done

    cd /srv/nfs/ngs-stockage/NGS_Bacteriologie/MTB/routine/${folder}/
    export TOWER_ACCESS_TOKEN="eyJ0aWQiOiA1MjQyfS4wYzU2ZGFiMmQxNjkxMzI4ZWVjNjIxYzNmYmNhOWUzMmQ5MmM4NTg3"
    /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/nextflow-22.04.0-all /srv/nfs/ngs-stockage/NGS_Bioinfo/Scripts/MTB/hcl_mtb_routine/main.nf -resume

    if [ -f results/multiqc/Couverture_mqc.yml ]; then
        warningCur=$(sed 1d results/multiqc/Couverture_mqc.yml | tr '\n' '@' | sed -re 's/[0-9]@/\n/g' | sed -re 's/:@.*Couv: /\t/' | sed -re 's/^    //' | awk '{if($2<=30){printf "Avertissement : l\047echantillon "$1" a une profondeur moyenne inferieure à 30X ("sprintf("%0.1f",$2)"X)."}}')
        warning=$warning$warningCur

        (echo -e "Hello,\n\nLa pipeline vient de tourner sur le run $folder.\n\n$warning\nBye.\n" && uuencode results/${folder}.xlsx ${folder}.xlsx && uuencode results/multiqc/${folder}_multiqc_report.html ${folder}_multiqc_report.html) | mail -s "Analyse de $folder" -a "From: di3225su <di3225su@chu-lyon.fr>" hcl_bioinfo@chu-lyon.fr oana.dumitrescu@chu-lyon.fr ext-charlotte.genestet@chu-lyon.fr elisabeth.hodille@chu-lyon.fr megane.pascal@chu-lyon.fr brune.joannard@chu-lyon.fr tiphaine.roussel-gaillard@chu-lyon.fr quentin.testard@chu-lyon.fr gerard.lina@chu-lyon.fr jean-philippe.rasigade@chu-lyon.fr houda.el-madani-hammanou@chu-lyon.fr
    fi

    if [ -d results/spomutscan ]; then
        echo "Sample ID,Instrument ID(s),Analysis authorized by,AssayResultTargetCode,ORAM,SpoBin,SpoOct,SIT,espece" > results/glims.csv

        sampleString=$(ls results/spollineages/*Amplicon*.csv | cut -f3 -d"/" | rev | cut -f2- -d "_" | rev | tr '\n' ',' | sed -re 's@,@","@g')
        sampleString=$(echo '"'${sampleString::-2})

        tabSQL=$(ssh di3183su "mysql bdd-ngs -N -B -e 'SELECT code_fastq,identifiant_glims FROM echantillon_projetserie WHERE code_fastq in ($sampleString)'" | grep -v NULL)

        for sample in $(echo "$tabSQL" | cut -f1); do
            paste -d "," <(echo "$tabSQL" | grep "$sample" | cut -f2) <(echo BIOMOLBK,elisabeth.hodille@chu-lyon.fr,SPOLIGO) <(echo "$sample") <(tail -n 1 results/spollineages/${sample}_S*csv | cut -f2 -d";" | sed -re 's@o@0@g' | sed -re 's@n@1@g') <(tail -n 1 results/spollineages/${sample}_S*csv | cut -f3 -d";" | tr -d '\047') <(tail -n 1 results/spollineages/${sample}_S*csv | cut -f13 -d";") <(tail -n 1 results/spollineages/${sample}_S*csv | cut -f10 -d";")
        done | sort -V | uniq >> results/glims.csv

        (echo -e "Hello,\n\nLa pipeline Amplicon vient de tourner sur le run $folder.\n\n$warning\nBye.\n" && uuencode results/glims.csv glims.csv) | mail -s "Analyse de $folder" -a "From: di3225su <di3225su@chu-lyon.fr>" hcl_bioinfo@chu-lyon.fr oana.dumitrescu@chu-lyon.fr ext-charlotte.genestet@chu-lyon.fr elisabeth.hodille@chu-lyon.fr megane.pascal@chu-lyon.fr brune.joannard@chu-lyon.fr tiphaine.roussel-gaillard@chu-lyon.fr quentin.testard@chu-lyon.fr gerard.lina@chu-lyon.fr jean-philippe.rasigade@chu-lyon.fr houda.el-madani-hammanou@chu-lyon.fr
    fi

    if [ ! -f results/multiqc/Couverture_mqc.yml ] && [ ! -d results/spomutscan ]; then
        (echo -e "Hello,\n\nLa pipeline vient de tourner sur le run $folder et il y a un problème.\n\n$warning\nBye.\n") | mail -s "Analyse de $folder en erreur" -a "From: di3225su <di3225su@chu-lyon.fr>" hcl_bioinfo@chu-lyon.fr quentin.testard@chu-lyon.fr
    fi

    echo "Fin de traitement de $folder"
    date
done

